<?php include('top.php'); ?>
<?php include('header.php'); ?>

<div class="row main-content">
  <div class="col-md-8">
      <?php include('articles.php'); ?>
  </div>
  <div class="col-md-3 calendar">
      <?php include('calendar.php'); ?>
  </div>
</div>

 <?php include('footer.php'); ?>
   <!-- Content End -->

<?php include('bottom.php'); ?>
