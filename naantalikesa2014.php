<div class="page">
    <h2> Naantalin kesäliigan tilastot kaudelta 2014</h2>

<?php

        include('sql.php');
        include('seasonstats.php');

        $SEASON_ID = 3;
        game_stats($SEASON_ID, $mysqli);
        ?>

<h2> Ranking </h2>
<h5> Kokonaispisteisiin lasketaan 15 parasta turnausta (lihavoidut)</h5>

<table class="ranking table-striped table-hover">

<?php
            ranking_stats($SEASON_ID, $mysqli);
?>
</table>

</div>
