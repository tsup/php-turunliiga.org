<div class="page">
    <h2> Turun liigan tilastot kaudelta 2016-2017</h2>

<?php

        include('sql.php');
        include('seasonstats.php');

        $SEASON_ID = 9;
        game_stats($SEASON_ID, $mysqli);
?>
<hr />

<h2> Ranking </h2>
<h5> Kokonaispisteisiin lasketaan 6 parasta turnausta (lihavoidut)</h5>

<table class="ranking table-striped table-hover">

<?php
    ranking_stats($SEASON_ID, $mysqli); 
?>
</table>

</div>
