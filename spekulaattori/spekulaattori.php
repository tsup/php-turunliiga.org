<?php

require('helpers.php');
require('stats_funcs.php');

$new = $_POST['sijoitukset'];
$new = explode("\n", $new);

$sanitized = array();
foreach($new as $name) {
    $sanitized[] = trim($name);
}

$points = read_csv('mm.csv');
$new_points = $points;
$i = 1;
foreach($sanitized as $name) {
    $pts = get_point($i++);
    $new_points[$name][6] = $pts;
    unset($new_points[$name]['total']);
    $new_points[$name]['total'] = calculate_points($new_points[$name]);
}
uasort($new_points, 'sort_by_total');


?>

<doctype !html>

<html>
    <head>
        <meta charset='utf8' />
        <title> SPJKL-maajoukkue spekulaattori </title>

        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">

        <link rel="stylesheet" href="style.css" />
    </head>
    <body>

    <div class="container">
    <div class="row">
        <div class="col-md-12">
            <h1> SPJKL-MM-spekulaattori </h1>

            <p> Tämä spekulaattori on tarkoitettu työkaluksi tilastonnälkäisille pöytäjääkiekkoilijoille. Spekulaattorista löytyy ranking-kauden kuuden ensimmäisen turnauksen tulokset ja voit syöttää loppusijoituslistan haluamassasi järjestyksessä päätöskilpailusta ja näet, miten se vaikuttaa lopullisiin sijoituksiin. Ei enää hankalia Excel-tiedostoja. Tämä työkalu on koodattu aamukahvin parissa, joten siinä saattaa olla pieniä virheitä ja sen design on mitä on. Tilastojen oikeellisuutta ei myöskään taata, joten reissulippuja ei näiden pohjalta kannata tehdä. Palautetta voi antaa <a href="https://facebook.com/poytajaakiekko">Facebookissa</a> tai sähköpostilla <code>webmaster at poytajaakiekko fi</code></p>

            <p> Uusi taulukko päivittyy sivun alalaitaan <code>Submit</code>-napin painamisen jälkeen. </p>
        </div>
    </div>
    <div class="row">
    <div id="preview" class="col-md-6">
    <h3> Tilanne 6 turnauksen jälkeen </h3>
    <p><i> Esikatselussa mukana 30 parasta. Lopullisiin lasketaan kuitenkin kaikki pelaajat </i></p>
    <table class="table table-condensed table-hover table-striped">
        <thead>
            <tr>
                <th> Sij. </th><th>Pelaaja</th><th>Yht.</th><th>Ou</th><th>He</th><th>Tu</th><th>SM</th><th>Ma</th><th>Na</th><th>Va</th>
            </tr>
        </thead>
        <tbody>
    <?php
            $i = 1;
            foreach(array_slice($points,0, 31) as $player => $pts) {
                echo '<tr>';
                echo '<td>'.$i++.'</td><td>'.$player.'</td><td>'.$pts['total'].'</td>';
                unset($pts['total']);
                foreach($pts as $tourn) {
                    echo '<td>'.$tourn.'</td>';
                }
            }
    ?>
    </table>
    </div>
    <div class="col-md-6" id="input">
    <h3> Syötä loppusijoituslista </h3>
    <p> <i>Syötä tähän Varkauden loppusijoituslista muodossa:</i> <br />
<pre>
    Suojanen Antti
    Lampi Ahti
    Pelkonen Jan
</pre>
    <i> Tarkista että nimet on kirjoitettu oikein eikä riveillä ole muita merkkejä.</i>
    <form name="varkaus" action="spekulaattori.php" method="post">
        <textarea class="form-control" cols="50" rows="20"  name="sijoitukset"><?php foreach($sanitized as $name) echo $name."\n" ?></textarea><br />
        <input type="submit" class="btn btn-default" />
    </form>
    </div>
</div>
<hr />
<div class="row">
<div class="col-md-6" id="results">
<h3> Tilanne 7 turnauksen jälkeen </h3>
<table class="table-condensed table-striped table-hover table">
    <thead>
        <tr>
            <th> Sij. </th><th>Pelaaja</th><th>Yht.</th><th>Ou</th><th>He</th><th>Tu</th><th>SM</th><th>Ma</th><th>Na</th><th>Va</th>
        </tr>
    </thead>
    <tbody>
<?php
        $i = 1;
        foreach(array_slice($new_points,0, 30) as $player => $pts) {
            echo '<tr>';
            echo '<td>'.$i++.'</td><td>'.$player.'</td><td>'.$pts['total'].'</td>';
            unset($pts['total']);
            foreach($pts as $tourn) {
                echo '<td>'.$tourn.'</td>';
            }
        }
?>
</table>
</div>
</div>

<div class="row">
<div class="col-md-12">
<footer>
    <a href="http://poytajaakiekko.fi">Suomen pöytäjääkiekkoliitto ry</a>
</footer>
</div>
</div>

    </body>
</html>
