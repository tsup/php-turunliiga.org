<!-- Main Content start-->
            <div class="main-content">
               <div class="container">
                    <h2> Tietoa jäsenyydestä</h2>
                    <p>Turun seudun pöytäjääkiekko ry:n jäsenmaksu vuodelle 2015 on 20 euroa.</p>

                    <p>Uudet jäsenet saavat jäsenmaksun hinnalla seuran pelipaidan. Muita jäsenetuja on mahdollisuus edustaa Turun seudun pöytäjääkiekkoa päätösviikonloppuna pelattavassa joukkueiden Suomen mestaruusturnauksessa sekä mahdolliset alennukset seuran järjestämissä turnauksissa ja turnausmatkoissa. </p>

                    <p>Jäseneksi voi liittyä täyttämällä <a href="jasenlomake.pdf">jäsenlomakkeen</a> ja toimittamalla sen turnauksien yhteydessä hallitukselle ja maksamalla hakemuslomakkeessa mainittujen ohjeiden mukaisesti jäsenmaksun. </p> 

                </div>
            </div>
            <!-- Main Content end-->
