<!-- Main Content start-->

    <?php

    include('sql.php');

    $query = "SELECT id, title, author, content, excerpt, cast(substring_index(cast(date as char),':',1) as date) as d FROM articles WHERE display = 1 ORDER BY id DESC LIMIT 5";
    $result = $mysqli->query($query);


    while($row = $result->fetch_assoc()) {
        $title = $row['title'];
        $date = date('j.n.Y', strtotime($row['d']));
        $excerpt = $row['excerpt'];
        $id = $row['id'];

		echo '<div class="row article-item">';
        echo '<div class="col-md-3 article-picture">';
          echo '<img src="img/turunliiga_place_holder.png" />';
        echo '</div>';
        echo '<div class="col-md-8">';
          echo "<h3>$title</h3>";
          echo "<p span='date'>$date</p>";
          echo "<p> $excerpt</p>";
          echo "<p class=\"pull-right\"> <a href=\"page.php?page=post&id=$id\"> Lue lisää <i class=\"icon-chevron-right\"></i></a></p>";
        echo '</div>';
      echo '</div>';
      echo '<hr />';
    }
    ?>

    <div class="pull-right">
      <h4 class="all-articles">
        <a href='page.php?page=all_articles'>Lue kaikki jutut <span class='icon-chevron-right'></span></a>
      </h4>
    </div>
