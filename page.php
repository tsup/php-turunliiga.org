<?php

$page_to_display = $_GET['page'];
$page_to_display .= '.php';

if (!file_exists($page_to_display)) {
    $page_to_display = '404.php';
}

?>


<?php include('top.php'); ?>
<?php include('header.php'); ?>

<div class="row main-content">
  <div class="col-md-8">
      <?php include($page_to_display); ?>
  </div>
  <div class="col-md-4 calendar">
      <?php include('calendar.php'); ?>
  </div>
</div>

 <?php include('footer.php'); ?>
   <!-- Content End -->

<?php include('bottom.php'); ?>
