<?php include('sql.php'); include('stats_funcs.php'); ?>

<!-- Main Content start-->
<div class="main-content">
    <div class="container">
        <h2> Joukkueranking </h2>
        <h4> JoukkueSM 2016-2017</h4>

        <p> Turun seudun pöytäjääkiekko jakaa ensi kevään joukkueiden Suomen mestaruuskilpailuihin pelaajansa kansallisen rankingin ja Turun liigan menestyksen mukaisesti.
            Turun liigasta lasketaan pelaajan 3 parhaan turnauksen tulokset ja ranking-turnauksista 5 parasta tulosta.</p>

<?php
    $turkuquery = "select player.name as name, t_id, points from player, rankingpoints WHERE t_id IN (SELECT tournament.id from tournament, season WHERE season.id = season_id and season.id = 9) AND player.id = p_id AND player.name NOT IN ('Jali Salo', 'Antti Suojanen', 'Kevin Eriksson', 'Vesa Virri', 'Olavi Väisänen', 'Marko Viljanen', 'Tommi Rokka', 'Petteri Nyholm', 'Petri Paavilainen', 'Vesa Huotelin', 'Ahti Lampi') order by player.name";
    $result = $mysqli->query($turkuquery);

    $spjklquery = "select nimi, tulos from spjkl2016 where nimi in (SELECT name from player) AND nimi not in ('Kevin Eriksson', 'Vesa Virri', 'Antti Suojanen', 'Jali Salo', 'Olavi Väisänen', 'Marko Viljanen', 'Tommi Rokka', 'Petteri Nyholm', 'Petri Paavilainen', 'Vesa Huotelin')";
    $spjklresult = $mysqli->query($spjklquery);
    $pelaajat = array();
    $spjklpelaajat = array();
    while ($row = $result->fetch_assoc()) {
        $name = $row['name'];
        if(!array_key_exists($name, $pelaajat)) {
            $pelaajat[$name] = array();
        }
        $pelaajat[$name][] = $row['points'];
    }
    foreach($pelaajat as $pelaaja => $points) {
        $pelaajat[$pelaaja]['total'] = array_sum($points);
        $pelaajat[$pelaaja] = count_best_n($pelaajat[$pelaaja], 3);
    }
    while ($row = $spjklresult->fetch_assoc()) {
        $name = $row['nimi'];
        if(!array_key_exists($name, $spjklpelaajat)) {
            $spjklpelaajat[$name] = array();
        }
        $spjklpelaajat[$name][] = $row['tulos'];
    }
    foreach($spjklpelaajat as $pelaaja => $points) {
        $spjklpelaajat[$pelaaja]['total'] = array_sum($points);
        $spjklpelaajat[$pelaaja] = count_best_n($spjklpelaajat[$pelaaja], 5);
    }

    $kaikki = array();
    foreach($pelaajat as $pelaaja => $points) {
        if(!array_key_exists($pelaaja, $kaikki)) {
            $kaikki[$pelaaja] = array();
        }
        $kaikki[$pelaaja]['turku'] = $points['total'];
    }
    foreach($spjklpelaajat as $pelaaja => $points) {
        if(!array_key_exists($pelaaja, $kaikki)) {
            $kaikki[$pelaaja] = array();
        }
        $kaikki[$pelaaja]['spjkl'] = $points['total'];
    }

    foreach($kaikki as $pelaaja => $points) {
        $kaikki[$pelaaja]['total'] = array_sum($points);
    }
    uasort($kaikki, 'sort_by_total');

   echo '<table class="joukkueranking"><tr><th> Nimi </th><th class="num">SPJKL</th><th class="num">Turun liiga</th><th class="num">Yht. </th></tr>';
    foreach($kaikki as $pelaaja => $points) {
        echo '<tr><td>' . $pelaaja . '</td><td class="num">' . $points['spjkl'] . ' </td><td class="num">' . $points['turku'] . '</td><td class="num">' . $points['total'] . '</td></tr>';
    }
    echo '</table>';
?>
    </div>
</div>
<!-- Main Content end-->
