<div class="page">
    <h2> Turun liigan tilastot kaudelta 2014-2015 </h2>

<?php

        include('sql.php');
        include('seasonstats.php');

        $SEASON_ID = 4;
        game_stats($SEASON_ID, $mysqli);
?>
<hr />
<h2> Grande Finale </h2>
<ol> 
    <li> 1. Kevin Eriksson</li>
    <li> 2. Jan Pelkonen </li>
    <li> 3. Rene Huhtala </li>
</ol>
<br />
<p> <a href="http://turunliiga.org/tournament_files/turunliiga_23052015.tnmt.html">Tulokset</a></p>

<hr />

<h2> Ranking </h2>
<h5> Kokonaispisteisiin lasketaan 6 parasta turnausta (lihavoidut)</h5>

<table class="ranking table-striped table-hover">

<?php
        ranking_stats($SEASON_ID, $mysqli);
?>
</table>

</div>
