
    <h1> Kaikki artikkelit </h1>
    <?php

    include('sql.php');

    $query = "SELECT id, title, author, content, excerpt, cast(substring_index(cast(date as char),':',1) as date) as d FROM articles ORDER BY id DESC";
    $result = $mysqli->query($query);
    while($row = $result->fetch_assoc()) {
      $title = $row['title'];
      $date = date('d.m.Y', strtotime($row['d']));
      $id = $row['id'];

      echo '<div class="row">';
        echo '<div class="col-md-12">';
          echo '<ul class="all-articles">';
            echo "<li> <span class='date'> $date</span>: <a href='page.php?page=post&id=$id'> $title</a></li>";
          echo '</ul>';
        echo '</div>';
      echo '</div>';
    }
?>
