<?php

function game_stats($season_id, $mysqli) {
        include('stats_funcs.php');

        $rank = 1;
        $tournament_count = get_tournament_count($season_id, $mysqli);
        $tournament = ($tournament_count == 1) ? 'turnaus' : 'turnausta';
        echo '<h3> Pelattu ' . $tournament_count . ' ' . $tournament . ' </h3>';
        $result = $mysqli->query("SELECT name FROM player");

        echo "<table class=\"players table-striped table-hover sortable\">\n";
        echo "<tr><th>Sij.</th><th>Nimi</th><th> O </th><th> V </th><th> T </th> <th> H </th><th>TM</th><th>PM</th><th>+/-</th><th>P</th><th class=\"separator\">&nbsp;</th><th> TM / O </th><th> PM / O </th><th> TM+PM / O </th></tr>";

        $query = get_stats_query($season_id);
        $stats = $mysqli->query($query);
while ($row = $stats->fetch_assoc()) {
            $class = ($row['goal_difference'] < 0) ? "class='neg'" : "";
        echo "<tr><td>" . $rank++ . "<td>" . $row['name'] . "</td><td> " . $row['games'] . "</td><td> " . $row['wins'] . "</td><td>" . $row['draws'] . "</td><td>" . $row['losses'] . "</td><td>" . $row['goals_for'] . "</td><td>" . $row['goals_against'] . "</td><td " . $class . ">" . $row['goal_difference'] . "</td><td>" . $row['points'] . "</td><td>&nbsp;</td><td>" . $row['scoredpergame'] . "</td><td>" . $row['againstpergame'] . "</td><td>" . $row['goalspergame'] . "</td></tr>";
    }
        echo "</table>";
}

function ranking_stats($season_id, $mysqli) {

    $CUTTER = 5;


    $player_names = get_players($season_id, $mysqli);
    $tournaments = get_tournaments($season_id, $mysqli);
    $player_info = array();

    $player_info = get_player_info($tournaments, $player_names, $season_id, $mysqli);
    $placement = 1;

    foreach($player_info as $player => $points) {
        $player_info[$player] = count_best_n($points,5);
    }
    //$player_info = array_map("count_best_n", $player_info, array_fill(0, count($player_info), 6));

    uasort($player_info, 'sort_by_total');
    $i = 1;
    $tournament_number = 1;
    echo '<tr><th> Sij. </th><th> Nimi </th>';
    foreach($tournaments as $t) {
        $fname = get_filename_by_tournament($t, $mysqli);
        echo '<th> <a href="tournament_files/' . $fname . '">#' . $tournament_number++ . '</a></th>';
    }
    if(count($tournaments) > $CUTTER) {
        echo '<th>' . $CUTTER .' / ' . count($tournaments) . '</th>';
    }
    else {
        echo '<th>Yht. </th>';
    }

    echo '</tr>';
    foreach($player_info as $player => $points) {
        $points = count_best_n($points, 5);
        $lim = get_limit($points, $CUTTER);
        $gt_lim_func = larger_than_limit($lim);
        $gt_lim_count = count(array_filter($points, $gt_lim_func));
        $max_lim = $CUTTER - $gt_lim_count;
        $lim_counter = 0;

        echo '<tr><td>' . $i++ . '</td><td>' . $player . '</td>';
        foreach($points as $t_id => $pts) {
            if($pts > 0 && ($pts > $lim || $pts == $lim && $lim_counter <= $max_lim)) {
                echo '<td><strong>' . $pts . '</strong></td>';
                if($pts == $lim) $lim_counter++;
            }
            else {
                echo '<td>' . $pts . '</td>';
            }
        }
        echo '</tr>';

        if ($i == 13) {
            echo '<tr> <td> &nbsp; </td></tr>';
        }
    }


}


