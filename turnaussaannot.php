<div class="page">

<h3> Turun liigan kausi- ja turnaussäännöt </h3>

<div class="rules">

<p><strong> Kausijärjestelmä </strong></p>
<ol>
<li> Turun liigassa pelataan 9 normaalia turnausta sekä kauden päättävä pudotuspeliturnaus.</li>
<li> Lopputurnaukseen pääsee 12 parasta pelaajaa siten, että kunkin pelaajan kuusi (6) parasta turnausta lasketaan rankingiin mukaan. Saadakseen osallistumisoikeuden lopputurnaukseen on pelaajan osallistuttava vähintään neljään (4) turnaukseen.</li>
<li> Lopputurnauksen yhteydessä pelataan myös alempi loppusarja, johon osallistumisoikeuden saavat kaikki vähintään kerran kauden aikana Turun liigaan osallistuneet.</li>
<li> Tasapisteissä sijoituksen ratkaisee paras yksittäinen turnaus, näiden ollessa tasan 2. paras, jne.</li>
<li><p>Lopputurnauksessa kaikki sarjat pelataan paras 7 -järjestelmällä ja 3 kauden parasta pelaajaa palkitaan asiaankuuluvin palkinnoin.</p></li>
</ol>

    <p><strong>Osallistumismaksut </strong></p>
<ol>
<li>Osallistumismaksu on 5 euroa/turnaus tai 20 euroa/kausi.</li>
<li><p>Kauden ensimmäinen turnaus on kullekin pelaajalle ilmainen.</p></li>
</ol>


    <p><strong>Ranking-pisteet </strong></p>
<ol>
<li><p>Kustakin turnauksesta on jaossa pisteitä seuraavasti:</p>

<p>30, 25, 20, 18, 15, 12, 10, 8, 5, 4, 3, 2, 1</p></li>
<li><p>Mikäli turnauksessa on osallistujia yli 12, kaikki loput pelaajat saavat yhden pisteen.</p></li>
</ol>


    <p><strong> Turnausjärjestelmä </strong></p>
<ol>

<li><em>Alle 7 pelaajaa </em><br />Pelataan kaksinkertainen runkosarja sekä pudotuspelit 4 parhaan kesken paras 7 -järjestelmällä.</li>
<li><em>7-15 pelaajaa</em><br />
Pelataan yksinkertainen runkosarja sekä 8 pelaajan pudotuspelit paras 7 -järjestelmällä.</li>
<li><em>16+ pelaajaa</em><br />
Pelataan kahdessa lohkossa. Pudotuspeleihin joko 8 tai 16 pelaajaa riippuen kokonaispelaajamäärästä.</li>
<li><em>Pronssiottelu</em><br />
Turnauksissa ei pelata pronssiottelua. Kolmas sija ratkaistaan runkosarjan sijoituksen perusteella.</li>
<li><p><em>Sijoitusottelut</em><br />
Pudotuspeleistä ulos jääneet pelaajat pelaavat sijoitusottelut (sijat 9-10, 11-12 jne). Tippuneiden kesken ei pelata erillistä alasarjaa.</p></li>
</ol>

    <p><strong>Muutokset sääntöihin </strong></p>
<ol><li> <em>Muutosoikeus</em><br />

<p>Turun seudun pöytäjääkiekko ry:n hallitus voi harkinnanvaraisesti viedä turnauksen läpi näistä turnaussäännöistä poiketen, kunhan tästä tiedotetaan turnaukseen osallistuville ennen ensimmäisen ottelun alkua.</p>

<p>Esimerkiksi 12-16 pelaajan turnauksessa voidaan pelata pudotuspeleistä pudonneiden kesken sijoitusotteluiden sijaan alempi jatkosarja.</p> </li>
</ol>


</div>
</div>
