<?php include('calendar-api.php'); include('sql.php'); ?>
<h3> <i class="icon-trophy"></i>  Viimeisimmät tulokset </h3>
<div class="calendar-inner">

  <?php
    $query = "SELECT * FROM tournament ORDER BY date DESC LIMIT 3";
    $result = $mysqli->query($query);

    $names = array();
    $names['turunliiga'] = 'Turun liiga ';
    $names['naantalinliiga'] = 'Naantalin liiga ';
    $names['naantalikesa'] = 'Naantalin kesäliiga ';
    echo "<ul class='last_tournaments'>";
    while ($row = $result->fetch_assoc()) {
      $name = $row['name'];
      $name = explode('_', $name);
      $display_name = $names[$name[0]];
      $display_date = explode('.', $name[1]);
      $display_date = $display_date[0];
      $day = substr($display_date, 0, 2);
      $month = substr($display_date, 2, 2);
      $year = substr($display_date, 4);
      $display_date = $year . "-" . $month . "-" . $day;

      $display_date = date('d.m.Y', strtotime($display_date));

      echo "<li><a href='tournament_files/" . $row['filename'] . "'>" . $display_date . ": " . $display_name . "</a><li>";
    }
    echo "</ul>";
  ?>
</div>
<h3> <a href="page.php?page=ohjeet" class="no-link"><i class="icon-calendar"> </i></a> Kisakalenteri </h3>
<div class="calendar-inner">
    <h5> Turun liigan seuraavat kisat </h5>
<?php

    function filter_turku($events) {
        return $events->colorId == 9;
    }

    function sort_by_date($a, $b) {
        return strcmp($a->start->date, $b->start->date);
    }

    $turku_events = array_filter($results->getItems(), 'filter_turku');
    echo "<ul>";
    if(empty($turku_events)) {
        echo "<li>Ei turnauksia kalenterissa.</li>";
    } else {
        foreach($turku_events as $event) {
            $start = $event->start;
            $start_d = new DateTime($start->date);
            echo "<li>". date_format($start_d, 'd.m.Y') . ": " . $event->summary . "</li>";
        }
	echo "</ul>";
    }
    ?>

    <h5> Naantalin liigan seuraavat kisat </h5>
    <?php
		function filter_naantali($events) {
			return $events->colorId == 11;
		}
        $naantali_events = array_filter($results->getItems(), 'filter_naantali');

        echo "<ul>";
    if(empty($naantali_events)) {
        echo "<li>Ei turnauksia kalenterissa.</li>";
    } else {
        foreach($naantali_events as $event) {
            $start = $event->start;
            $start_d = new DateTime($start->date);
            echo "<li>". date_format($start_d, 'd.m.Y') . ": " . $event->summary . "</li>";
        }
    }
		echo "</ul>";
?>


      <h5><a href="http://poytajaakiekko.net" target=_blank>SPJKL-rankingit</a></h5>
<?php
     	function filter_spjkl($events) {
			return $events->colorId == 5;
		}
        $spjkl_events = array_filter($results->getItems(), 'filter_spjkl');

	 echo "<ul>";
    if(empty($spjkl_events)) {
        echo "<li>Ei turnauksia kalenterissa.</li>";
    } else {
        foreach($spjkl_events as $event) {
            $start = $event->start;
            $start_d = new DateTime($start->date);
            echo "<li>". date_format($start_d, 'd.m.Y') . ": " . $event->summary . "</li>";
        }
    }
		echo "</ul>";
            ?>


      <p class="pull-right" style="margin-right: 30px; margin-top: 15px;"><span class="footnote"> (Muutokset turnauksiin mahdollisia.) <span></p>
    <div style="clear: both;"></div>
    <h3> <i class="icon-group"></i>Toimintaamme tukee </h3>
    <a href="http://airistonautovuokraus.com" target=_blank><img src="img/sponsors/airisto-line.png" width="300px"/></a>
    <hr />
    <a href="https://www.dnvgl.com/" target=_blank><img src="img/sponsors/DNV.png" width="300px"/></a>
</div>
