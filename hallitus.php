<!-- Main Content start-->
            <div class="main-content">
               <div class="container">
                    <h2> Turun seudun pöytäjääkiekko ry. - Hallitus </h2>
                    <p>
                        Turun seudun pöytäjääkiekko ry:n toimintaa pyörittää viishenkinen hallitus, joka valitaan vuosittain yhdistyksen sääntömääräisessä syyskokouksessa. Tällä hetkellä toimintaa luotsaa seuraava nelikko:</p>
                        <p>
                        Jouko Pelkonen (<em>puheenjohtaja</em>)<br />
                        Jan Pelkonen (<em>sihteeri</em>)<br />
                        Janne Ollila<br />
                        Janne Kantola<br />
                        Kaj Carlsson<br />
                        Rene Huhtala (<em>Varajäsen</em>)<br />
                        Kevin Eriksson(<em>Varajäsen</em>)<br />
                    </p>

                    <p> Verkkosivuista sekä tilastotekniikasta vastaa Juha-Matti Santala

                    </p>

                </div>
            </div>
            <!-- Main Content end-->
