<!DOCTYPE html>
<!--[if IE 8]>			<html class="ie ie8"> <![endif]-->
<!--[if IE 9]>			<html class="ie ie9"> <![endif]-->
<!--[if gt IE 9]><!-->	<html> <!--<![endif]-->
   <head>
      <meta charset="utf-8">
      <title>Turun seudun pöytäjääkiekko</title>
      <meta name="description" content="Turun seudun pöytäjääkiekko ry">
      <meta name="author" content="Turun seudun pöytäjääkiekko">
      <meta property="og:title" content="Turun seudun pöytäjääkiekko ry"/>
      <meta property="og:type" content="website"/>
      <meta property="og:url" content="http://turunliiga.org"/>
      <meta property="og:description" content="Turun seudun pöytäjääkiekko ry. on perustettu 2013 parantamaan pöytäjääkiekon tilaa harrastuksena pääosin Turun, Raision ja Naantalin alueella. Paikallistasolla pyöritämme paikallisliigoja Naantalissa ja Turussa, kansallisella tasolla järjestämme rankingturnauksia sekä Suomen mestaruustason turnauksia ja kansainvälisesti olemme mukana järjestämässä turnauksia Suomessa yhdessä muiden seurojen sekä lajin kattojärjestön kanssa."/>
      <meta property="og:image" content="http://turunliiga.org/img/tsup-fb.png" />
      <!-- Mobile Metas -->
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <!-- Google Fonts -->
      <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700,800' rel='stylesheet' type='text/css'>
      <link href='http://fonts.googleapis.com/css?family=Oswald:400,300,700' rel='stylesheet' type='text/css'>
      <!-- Library CSS -->
      <link rel="stylesheet" href="css/bootstrap.css">
      <link rel="stylesheet" href="css/fonts/font-awesome/css/font-awesome.css">
      <link rel="stylesheet" href="css/animations.css" media="screen">
      <link rel="stylesheet" href="css/superfish.css" media="screen">
      <link rel="stylesheet" href="css/nivo-slider.css" media="screen">
      <link rel="stylesheet" href="css/prettyPhoto.css" media="screen">
      <!-- Theme CSS -->
      <link rel="stylesheet" href="css/style.css">
      <!-- Skin -->
      <link rel="stylesheet" href="css/colors/blue.css">
      <!-- Responsive CSS -->
      <link rel="stylesheet" href="css/theme-responsive.css">
      <!-- Favicons -->
      <link rel="shortcut icon" href="img/ico/favicon.ico">
      <link rel="apple-touch-icon" href="img/ico/apple-touch-icon.png">
      <link rel="apple-touch-icon" sizes="72x72" href="img/ico/apple-touch-icon-72.png">
      <link rel="apple-touch-icon" sizes="114x114" href="img/ico/apple-touch-icon-114.png">
      <link rel="apple-touch-icon" sizes="144x144" href="img/ico/apple-touch-icon-144.png">
      <!-- The HTML5 shim, for IE6-8 support of HTML5 elements -->
      <!--[if lt IE 9]>
      <script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
      <script src="js/respond.min.js"></script>
      <![endif]-->
      <!--[if IE]>
      <link rel="stylesheet" href="css/ie.css">
      <![endif]-->

    <script src="http://code.jquery.com/jquery-1.10.2.min.js"></script>
    <link rel="stylesheet" href="css/stats.css" />
    <script src="js/instafeed.min.js"></script>
   </head>

