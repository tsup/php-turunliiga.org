<div style="margin-left: 45px; margin-top: 20px; margin-bottom: 20px; margin-right: 45px;">
<h2>Viralliset säännöt pelilaudalla</h2>

<p> Turun seudun pöytäjääkiekon turnauksissa noudatetaan <a href="http://www.poytajaakiekko.fi"> Suomen pöytäjääkiekkoliiton</a> virallisia pelisääntöjä.</p>

<h3>0. Määritelmiä</h3>
<p>
Näissä säännöissä sanat <em>kilpailija</em> ja <em>vastustaja</em>
tarkoittavat ihmistä, joka pelaa pöytäjääkiekkopeliä, ja sana <em>pelaaja</em>
tarkoittaa pöytäjääkiekkopeliin kuuluvaa, etäisesti kiekkoilijan näköistä
osaa, jota kilpailija ohjaustankojen avulla pelin aikana voi liikuttaa.
</p>

<img src='img/playoff.jpg' width='229' height='112' border='0' align='right'>
<h3>1. Pelivälineet </h3>
<p>
Virallinen kilpailumalli on <b>Stiga Play Off</b> -pöytäjääkiekkopeli,
josta muoviset maalikupit on korvattu kangaspusseilla.
</p>

<h3>2. Pelivälineiden säätäminen ja korjailu </h3>
<p>
Koska kentällä olevat pelaajat liukuvat kiinnikettä pitkin ylös pelin
aikana, on sallittua painaa niitä alaspäin silloin, kun omalla
pelaajalla on kiekko hallussaan. Kilpailijalla on oikeus vaatia, että vastustaja
painaa omia pelaajiaan alas. Peliä saa jatkaa vasta, kun vastustaja on valmis.
Pelaajien on oltava luonnollisilla paikoillaan (ts. maalivahtia ei saa käyttää
kenttäpelaajana eikä päinvastoin).
</p>

<h3>3. Pelaajien vaihto </h3>
<p>
Omien pelaajien käyttö on sallittua, jos vastustaja antaa siihen luvan.
Pelaajia ei saa käsitellä mitenkään muuten kuin maalaamalla. Käyrä maila,
suurennettu maalivahti tms. johtaa heti pelaajan hylkäämiseen (pelaaja on
korvattava käsittelemättömällä pelaajalla) ja tuomaristo voi lisäksi
rangaista kilpailijaa.
</p>

<h3>4. Ottelun pituus </h3>
<p>
Virallisen ottelun kokonaispeliaika on 5 minuuttia.
Kello käy, vaikka kiekko olisi ulkona kentältä.
</p>

<h3>5. Aloitukset</h3>
<p>
Aloitukset tapahtuvat aina kentän keskipisteestä. Ottelun alkaessa kiekko on
kentän keskipisteessä.  Peli alkaa aloitusmerkistä. Ennen aloitusta on
maaleista otettava kaikki kiekot pois.
</p>
<p>
Kilpailijalla on oikeus tehdä aloitus, kun vastustaja on tehnyt maalin tai
jos vastustaja on lyönyt kiekon yli laidan. Aloituksessa kiekko on pidettävä
selvästi näkyvissä kentän keskipisteen kohdalla litteä puoli jäätä kohden,
n. 2 cm keskushyökkääjien yläpuolella. Kiekko pudotetaan suoraan alaspäin.
Keskushyökkääjien on ennen aloitusta oltava liikkumatta omilla
kenttäpuoliskoillaan.
</p>
<p>
Jatkoajan kaikki aloitukset tekee puolueeton henkilö.
</p>
<p>
Kaikissa aloituksissa on huomioitava, että vastustaja on valmis. Jos
vastustaja tekee huonon aloituksen, on kilpailijalla oikeus pyytää uusi aloitus.
Pelikatkoksen*) jälkeen tehdään aina aloitus mutta jos toisella kilpailijalla on
ollut selvästi kiekko oman pelaajansa hallussa jatketaan peliä siltä paikalta,
jossa kiekko oli ennen katkosta.
</p>
<p>
*) Pelikatkos syntyy mm. kun maali irtoaa paikaltaan, pelaaja irtoaa tapistaan,
vieras esine tulee pelikentälle tms.
</p>
<p>
Jos ottelussa on tuomari, tekee hän kaikki aloitukset.
</p>

<h3>6. Maali </h3>
<p>
Maali hyväksytään vain, jos kiekko jää maalin sisälle.
</p>
<p>
Maalin saa tehdä vasta
kolmen (3) sekunnin kuluttua aloituksen jälkeen.
Jos kiekko on aloituksen jälkeen ollut koko ajan keskushyökkääjän ulottuvilla,
ei keskushyökkääjällä kuitenkaan saa tehdä maalia suoraan edes kolmen sekunnin jälkeen.
</p>
<p>
Loppusummerin aikana syntynyt maali hylätään.
</p>
<p>
Jos pelaaja tai maalivahti menee rikki maalin yhteydessä, maali hyväksytään.
</p>

<h3>7. Saarto </h3>
<p>
Jos kiekko pysähtyy maalinjalle, on oikeus käyttää saartosääntöä. Tämä
tapahtuu siten, että kilpailija, jonka maalin luona kiekko on, irroittaa
otteensa maalivahdin vivusta ja sanoo "saarto"
tai "block". Jos maalivahti lyö kiekon omaan maalin ennen kuin on
"saarto/block" sanottu, maali hyväksytään.
</p>
<p>
Saarron jälkeen peli jatkuu aloituksella.
</p>

<h3>8. Kovat otteet </h3>
<p>
On kiellettyä vetää/työntää vipuja niin lujaa, että kenttä liikkuu tai peli
siirtyy paikaltaan. Iskulyönti, jossa suurella voimalla työnnetään vipu tai vivut
eteenpäin, on siis kokonaan kielletty. Tällaisilla tavoilla saatu maali
hylätään ja kilpailija voi saada tuomaristolta varoituksen.
Kilpailija voi saada myös varoituksen, jos hän kovilla otteillaan rikkoo pelikentän,
metallitankoja tms. Lisäksi kilpailija on velvollinen korvaamaan aiheuttamansa
vahingot.
</p>

<p>
Jos kilpailija saa pelin aikana itselleen etua tai jos hän estää
vastustajan pelaamista esimerkiksi
ravistamalla tai tärisyttämällä peliä, on vastustajalla oikeus pyytää
aloitus ja vaatia pelaajalle varoitusta. Jos vastustaja on hävinnyt selvän
kiekon hallussapidon on hänellä oikeus laittaa kiekko samaan paikkaan.
</p>

<h3>9. Viiden sekunnin sääntö ja ajanpeluu</h3>
<p>
Pelaaja saa pitää kiekkoa korkeintaan 5 sekuntia kerrallaan. Sen jälkeen täytyy
kiekko syöttää tai laukaista; muuten kilpailija syyllistyy ajanpeluuseen.
<p>Ajanpeluuksi katsotaan myös toistuva
edestakainen syöttäminen kahden pelaajan välillä.
</p>
<p>
Ajanpeluusääntöjä on kuitenkin vaikea kontrolloida ja siksi kilpailijan, jonka mielestä vastustaja
hidastelee, on huomautettava tästä vastustajalle. Mikäli ajanpeluu
jatkuu edelleen, kilpailija voi huomauttaa asiasta kilpailun tuomaristolle. Jos
tuomaristo on samaa mieltä, se antaa ajanpelaajalle varoituksen.
</p>
<p>
Tärkeintä kuitenkin on, että kumpikin kilpailija yrittää koko ajan tehdä jotain rakentavaa ja viedä peliä eteenpäin.
</p>
<p>
Jatkuva tahallinen kiekon pois laukaisu pelikentältä on kielletty ja
saattaa johtaa tuomariston antamaan varoitukseen.
</p>

<h3>10. Luistinlaukaussääntö </h3>
<p>
Pelaaja saa tehdä maalin jaloilla seuraavissa tapauksissa:
</p>
<ul>
<li>Laukauksella liikkuvasta kiekosta.
<li>"Vapaalla" (oikealla) jalalla saa sen lisäksi tehdä maalin myös
pysähtyneestä kiekosta pelaajavipua kiertämällä.
<li>Laukaus suoraan maaliin tai maalivahdin kautta maaliin ei ole
sallittua pysähtyneestä kiekosta työntämällä pelaajan jalalla. Työnnöllä
tarkoitetaan, että koko pelaaja liikkuu pelin pituussuunnassa tarkoituksella
työntää kiekkoa. Jos kiekko tällaisen laukauksen jälkeen sitävastoin selvästi
muuttaa suuntaa reunasta tai omasta pelaajasta (esim. pelkkä ohjaus
keskushyökkääjällä ei riitä) ja menee maalin, maali hyväksytään.
</p>
<p>
<li>Poikkeus:
Ensimmäisellä kosketuksella saa tehdä maalin, jos kiekko työnnetään jalalla
maaliin täsmälleen siitä paikasta, johon se on
pysähtynyt. Pelaaja ei siis saa "asetella" kiekkoa ennen laukausta.
</ul>
</p>

<h3>11. Sääntöriita</h3>
<p>
Jos kilpailijat eivät pysty sopimaan pelin aikana jostain, toinen kilpailijoista
voi sanoa "sääntöriita" tms. vastustajalle. Tapaus otetaan puheeksi ottelun jälkeen,
jos on tarvetta.
</p>

<h3>12. Ottelun keskeytys</h3>
<p>
Ottelu keskeytetään myöhempää jatkamista varten, jos toinen kilpailijoista
loukkaantuu niin, ettei peliä voida jatkaa heti (max. 5 min kuluessa) tai
jos peli hajoaa niin, ettei sitä pystytä heti korjaamaan tai vaihtamaan
uuteen peliin.
</p>

<p>
Jatkettaessa ottelua keskeytyksen jälkeen pitävät pelaajat tekemänsä maalit.
</p>

<h3>13. Kirppusääntö</h3>
<p>
Maalia ei saa tehdä suoraan *) kummallakaan puolustajalla tai
maalivahdilla, jos kiekko painetaan maalikehikkoa
vasten ja ammutaan siitä maaliin. Sama pätee, jos puolustajapelaaja painaa
kiekon maalivahtia vasten (tai päinvastoin).
</p>

<p>
*) Jos kiekko ammutaan laidan kautta maaliin, maali hyväksytään.
Maali hyväksytään myös, jos oman puolen
pelaajista joku muu kuin keskushyökkääjä selvästi muuttaa kiekon suuntaa.
</p>
</div>
