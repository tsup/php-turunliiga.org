         <!-- Footer Start -->
         <footer id="footer">
            <!-- Footer Top Start -->
            <div class="footer-top">
               <div class="container">
                  <div class="row">
                     <section class="col-lg-3 col-md-3 col-xs-12 col-sm-3 footer-one">
                        <h3>Turun seudun pöytäjääkiekko ry.</h3>
                        <p>
                            Turun seudun pöytäjääkiekko ry. on Naantalissa ja Turussa toimiva pöytäjääkiekkoilun kattojärjestö, joka organisoi ja järjestää toimintaa niin paikallis- kuin kansallisellakin tasolla.
                        </p>
                     </section>
                    <!--
                     <section class="col-lg-3 col-md-3 col-xs-12 col-sm-3 footer-two">
                        <h3>Twitter Stream</h3>
                        <ul id="tweets">
                        </ul>
                     </section>
                    -->
                     <section class="col-lg-3 col-md-3 col-xs-12 col-sm-3 footer-three">
                        <h3>Yhteystiedot</h3>
                        <ul class="contact-us">
                           <li>
                              <i class="icon-map-marker"></i>
                              <p>
                                 <strong>Turun liiga:</strong> Kupittaan jäähalli<br />
                                 Hippoksentie, Turku <br />
                                 <strong>Naantalin liiga:</strong> Vapaa-aikatulli <br />
                                 Henrikinkatu 7, Naantali
                              </p>
                           </li>
                           <li>
                              <i class="icon-phone"></i>
                              <p><strong>Jouko Pelkonen:</strong> 050 548 0728 </p>
                           </li>
                           <li>
                              <i class="icon-envelope"></i>
                              <p><strong>Email:</strong>jouko.k.pelkonen@gmail.com</p>
                           </li>
                        </ul>
                     </section>
                     <section class="col-lg-3 col-md-3 col-xs-12 col-sm-3 footer-four">
                        <h3><a href="https://www.flickr.com/photos/98499893@N03/"> Kuvia tapahtumista </h3>
                        <ul id="flickrfeed" class="thumbs"></ul>
                     </section>
                  </div>
               </div>
            </div>
            <!-- Footer Top End -->
            <!-- Footer Bottom Start -->
            <div class="footer-bottom">
               <div class="container">
                  <div class="row">
                     <div class="col-lg-6 col-md-6 col-xs-12 col-sm-6 "> &copy; Turun seudun pöytäjääkiekko 2013 | Layout: <a href="https://wrapbootstrap.com/theme/pixma-responsive-multipurpose-template-WB0B348C6">Pixma</a> </div>
                     <div class="col-lg-6 col-md-6 col-xs-12 col-sm-6 ">
                        <ul class="social social-icons-footer-bottom">
                           <li class="facebook"><a href="https://www.facebook.com/turunliiga"><i class="icon-facebook"></i></a></li>
                           <li class="twitter"><a href="https://www.twitter.com/turunliiga"><i class="icon-twitter"></i></a></li>
                           <li class="flickr"><a href="https://www.flickr.com/photos/98499893@N03/"><i class="icon-flickr"></i></a></li>
                           <li class="youtube"><a href="https://www.youtube.com/user/PaimionKeikkoilijat"><i class="icon-youtube"></i></a></li>
                                                </ul>
                     </div>
                  </div>
               </div>
            </div>
            <!-- Footer Bottom End -->
         </footer>
         <!-- Scroll To Top -->
         <a href="#" class="scrollup"><i class="icon-angle-up"></i></a>
      </div>
      <!-- Wrap End -->
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
      js.src = "//connect.facebook.net/en_GB/sdk.js#xfbml=1&appId=314017718630995&version=v2.0";
      fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
