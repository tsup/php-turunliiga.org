   <header id="header">

   <div class="row top-bar">
      <div class="col-md-7">
         <a><i class="icon-phone"></i> Ota yhteyttä: 050 548 0728 (pj Jouko Pelkonen)</a>
         <a href="mail@example.com"><i class="icon-envelope"></i> Email : jouko.k.pelkonen@gmail.com</a>
      </div>
      <div class="col-md-5">
         <div class="pull-right">
            <ul class="social pull-left">
               <li class="facebook"><a href="https://www.facebook.com/turunliiga"><i class="icon-facebook"></i></a></li>
               <li class="twitter"><a href="https://www.twitter.com/turunliiga"><i class="icon-twitter"></i></a></li>
               <li class="flickr"><a href="https://www.flickr.com/photos/98499893@N03/"><i class="icon-flickr"></i></a></li>
               <li class="instagram"><a href="https://instagram.com/turunliiga"><i class="icon-instagram"></i></a></li>
               <li class="youtube"><a href="https://www.youtube.com/user/PaimionKeikkoilijat"><i class="icon-youtube"></i></a></li>
            </ul>
         </div>
      </div>
   </div>




   <div class="main-header">
      <div class="container">
         <div class="topnav navbar-header">
            <a class="navbar-toggle down-button" data-toggle="collapse" data-target=".slidedown">
            <i class="icon-angle-down icon-current"></i>
            </a>
         </div>
         <div class="logo pull-left">
            <h1>
               <a href="index.php">
               <img src="img/logo.png" alt="logo" width="125" height="60" /> Turun seudun pöytäjääkiekko
               </a>
            </h1>
         </div>
         <div class="mobile navbar-header">
            <a class="navbar-toggle" data-toggle="collapse" href=".navbar-collapse">
            <i class="icon-reorder icon-2x"></i>
            </a>
         </div>
         <nav class="collapse navbar-collapse menu">
            <ul class="nav navbar-nav sf-menu">
               <li>
                  <a  href="index.php">
                  Etusivu
                  </a>
               <li>
                  <a href="#" class="sf-with-ul">
                   Yhdistys
                  <span class="sf-sub-indicator">
                  <i class="icon-angle-down "></i>
                  </span>
                  </a>
                  <ul>
                     <li><a href="page.php?page=hallitus" class="sf-with-ul">Hallitus</a></li>
                     <li><a href="page.php?page=jasenyys" class="sf-with-ul">Jäsenyys</a></li>
                                                </ul>
               </li>

               <li><a href="#" class="sf-with-ul">Säännöt <span class="sf-sub-indicator"><i class="icon-angle-down"></i></span></a>
               <ul>
                   <li> <a href="page.php?page=pelisaannot" class="sf-with-ul">Pelisäännöt </a></li>
                   <li> <a href="page.php?page=turnaussaannot" class="sf-with-ul">Turnaussäännöt</a></li>
               </ul>
               </li>
               <li>
                  <a href="#" class="sf-with-ul">
                  Tulokset
                  <span class="sf-sub-indicator">
                  <i class="icon-angle-down "></i>
                  </span>
                  </a>
                  <ul>
                     <li style="text-align: center;"> Kausi 2017-2018</li>
                     <!--li><a href="page.php?page=joukkueranking2016" class="sf-with-ul"> Joukkueranking 2016-2017</a></li-->
                     <li><a href="page.php?page=turku2017" class="sf-with-ul">Turun liiga 2017-2018</a></li>
                     <!--li><a href="page.php?page=naantali2016" class="sf-with-ul">Naantalin liiga 2016-2017</a></li-->
                      <li style="text-align: center;"> Kausi 2016-2017</li>
                     <li><a href="page.php?page=joukkueranking2016" class="sf-with-ul"> Joukkueranking 2016-2017</a></li>
                     <li><a href="page.php?page=turku2016" class="sf-with-ul">Turun liiga 2016-2017</a></li>
                     <li><a href="page.php?page=naantali2016" class="sf-with-ul">Naantalin liiga 2016-2017</a></li>
                     <li style="text-align: center;"> Kausi 2015-2016 </li>
                     <li><a href="page.php?page=joukkueranking2015" class="sf-with-ul"> Joukkueranking 2015-2016</a></li>
                     <li><a href="page.php?page=turku2015" class="sf-with-ul">Turun liiga 2015-2016</a></li>
                     <li><a href="page.php?page=naantali2015" class="sf-with-ul">Naantalin liiga 2015-2016</a></li>
                     <li style="text-align: center;"> Kausi 2014-2015 </li>
                     <li><a href="page.php?page=joukkueranking" class="sf-with-ul"> Joukkueranking 2014-2015</a></li>
                     <li><a href="page.php?page=naantalikesa2015" class="sf-with-ul">Naantalin kesäliiga 2015</a></li>
                     <li><a href="page.php?page=turku2014" class="sf-with-ul">Turun liiga 2014-2015</a></li>
                     <li><a href="page.php?page=naantali2014" class="sf-with-ul">Naantalin liiga 2014-2015</a></li>
                     <li style="text-align: center;"> Kausi 2013-2014 </li>
                     <li><a href="page.php?page=turku2013" class="sf-with-ul">Turun liiga 2013 - 2014</a></li>
                     <li><a href="page.php?page=naantali2013" class="sf-with-ul">Naantalin liiga 2013 - 2014</a></li>
                     <li><a href="page.php?page=naantalikesa2014" class="sf-with-ul">Naantalin kesäliiga 2014</a></li>
                  </ul>
               </li>
               <li> <a href="http://poytajaakiekko.fi/tilastot/turunliiga/live/?C=M;O=D" class="sf-with-ul"> LIVE </a></li>
            </ul>
         </nav>
      </div>

</header>
