<!-- Main Content start-->
            <div class="main-content">
               <div class="container">
                    <h2>Kalenterin käyttöohjeet</h2>
                    <p>
                        Näiden verkkosivujen oikean palstan kalenteri käyttää Google-kalenteria, johon saat käyttöoikeuden webmasterilta juhamattisantala@gmail.com.
                    </p>
                    <p>
                        Kaikki tapahtumat tulee tehdä kalenteriin koko päivä -tapahtumiksi (klikkaa tapahtuman muokkaussivulla Koko päivä -täppä päälle). Tapahtuman nimi näkyy kalenterissa sellaisenaan eli siihen voi laittaa kellonaikoja esille.</p>

                    <p> Turun liigan turnaukset käyttävät väriä <strong>tummansininen</strong>, Naantalin liiga <strong>tummanpunainen</strong> ja kansalliset ranking-turnaukset <strong> keltainen</strong>. Värin nimen näet pitämällä hiirtä väripalettien päällä.</p>


                </div>
            </div>
            <!-- Main Content end-->
